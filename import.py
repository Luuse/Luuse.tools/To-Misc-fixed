# /usr/bin/env python

import fontforge
import os
import glob
import sys
import lxml.etree as etree

fontFile = sys.argv[1]
size = sys.argv[2]
clean = sys.argv[3]

name = fontFile.split("/")[-1]
name = name.split(".")[0]
name = name + '-fixed-' + size

SVG_DIR = glob.glob('tmp/svg/*.svg')
font = fontforge.open(fontFile)
font.em = int(size) * 10


def clean_write(letter_file):
    parser = etree.XMLParser(remove_blank_text=True)
    svg = open(letter_file, "r")
    elem = etree.parse(svg, parser)
    root = elem.getroot()
    path = root.findall('path')
    root.remove(path[0])
    root = str(etree.tostring(root))
    root = root.replace("b'<", "<?xml version='1.0' standalone='yes'?><")
    root = root.replace(">'", ">")

    f = open(letter_file, 'w')
    f.write(root)
    f.close()


for glyph in SVG_DIR:
    if clean == '-clean=on':
        print(clean)
        clean_write(glyph)

    letter = glyph.split("/")[-1].replace(".svg", "")
    print(letter)
    zone = font[letter]
    chasse = zone.width
    zone.clear()
    letter_char = font.createMappedChar(letter)
    letter_char.width = chasse
    letter_char.importOutlines(glyph)

os.mkdir('output/' + name)
font.em = int(size)
font.generate('output/' + name + '/' + name + '.sfd')
font.generate('output/' + name + '/' + name + '.ttf')
font.generate('output/' + name + '/' + name + '.otf')
