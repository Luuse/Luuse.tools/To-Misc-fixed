#!/usr/bin/env bash

fontFile=$1
size=$2

rm -f tmp/bd/*.xbm
# rm -f tmp/svg/*.svg

python export.py $fontFile $size

# ./generate.sh

bash path.sh

# rm -f tmp/svg/111411*

python import.py $fontFile $size -clean=on
