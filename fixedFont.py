#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image
import svgwrite
import xml.etree.ElementTree as ET
import os
import sys
import glob
import fontforge

def find_rec(node, element, result):
    for item in node.findall(element):
        result.append(item)
        find_rec(item, element, result)
    return result

class get:

    def path(FileGet):
        result = []
        xml = ET.parse(FileGet)
        svg = xml.getroot()
        w = svg.attrib['width']
        h = svg.attrib['height']
        for item in svg.findall('.//{http://www.w3.org/2000/svg}path'):
            result.append(item) 

        return {"d":result[0].attrib['d'], "width": int(w), "height": int(h)}


class load:

    def font(self, FileFont):
        global FontOpen
        FontOpen = fontforge.open(FileFont)

class export: 

    def letters(self, Size=100, Output="output/bitmap/"):
        try:
           Size 
        except NameError:
            print("well, it WASN'T defined after all!")
        else:
            for gly in FontOpen.glyphs():
                gly.export(Output + gly.glyphname + ".xbm", int(Size))
                print(gly.glyphname)


class generate:

    def svg(self, Type, Folder, width=1, height=1, rotate=0, pth='null'):
        try:
            Folder
        except NameError:
            print("well, it WASN'T defined after all!")
        else:
            for letter in glob.glob(Folder):
                name = letter.split("/")[-1].replace('.xbm', '')
                im = Image.open(letter, 'r')
                dwg = svgwrite.Drawing(name + '.svg', size=(im.width, im.height))
                dwg.viewbox(0, 0, im.width, im.height)
                listPix = list(im.getdata())
                dataPixels = []
                count = 0

                for i in range(im.height):
                    for u in range(im.width):
                        if listPix[count] == 255:

                            if Type == 'rect': 
                                part = dwg.rect((u,i), (width, height))
                                if rotate != 0:
                                    part.rotate(rotate, center=(u + (width / 2), i + (height / 2)))
                                dwg.add(part)

                            elif Type == 'ellipse': 
                                part = dwg.ellipse(center=(u, i), r=(width, height))
                                if rotate != 0:
                                    part.rotate(rotate, center=(u + (width / 2), i + (height / 2)))
                                dwg.add(part)

                            elif Type == 'path': 

                                if i % 2 == 0:
                                    x = u + 1
                                    y = i
                                    part = dwg.path(d=pth[0]['d'], transform='translate('+ str(x) +','+ str(y) +')')
                                else:
                                    x = u + 1
                                    y = i
                                    part = dwg.path(d=pth[1]['d'], transform='translate('+ str(x) +','+ str(y) +')')

                                if rotate != 0:
                                    part.rotate(rotate, center=(u + (width / 2), i + (height / 2)))

                                dwg.add(part)

                        count = count + 1

                dwg.saveas('output/svg/' + name + '.svg')
                print('['+name+'] >> svg saved !')


    def font(self, OutputName, SvgDir='output/svg/*.svg'):
        SvgDir = glob.glob(SvgDir)
        print(FontOpen)
        for gly in SvgDir:
            glyname = gly.split("/")[-1].replace(".svg", "")
            zone = FontOpen[glyname]
            glywidth = zone.width
            zone.clear()
            zone.importOutlines(gly, 'removeoverlap')
            zone.width = glywidth
            zone.removeOverlap()
            print('fontforge added : [' + glyname + ']')

        os.makedirs('output/' + OutputName)
        FontOpen.generate('output/' + OutputName + '/' + OutputName + '.sfd')
        FontOpen.generate('output/' + OutputName + '/' + OutputName + '.ttf')
        FontOpen.generate('output/' + OutputName + '/' + OutputName + '.otf')
        FontOpen.close()
