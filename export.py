#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import fontforge
import glob
from PIL import Image
import svgwrite
import shutil
import os


# option = sys.argv[2]

# VARIABLES GLOBALES


# LES FONCTIONS
def buildSvg(inputFile):
    # print(inputFile)
    im = Image.open('tmp/bd/' + inputFile + '.xbm', 'r')
    listPix = list(im.getdata())
    dataPixels = []
    count = 0

    dwg = svgwrite.Drawing(inputFile + '.svg', size=(im.width, im.height))
    dwg.viewbox(0, 0, im.width, im.height)

    for i in range(im.height):
        for u in range(im.width):
            dataPixels += [(listPix[count], u, i)]
            count = count + 1

    rot = 45
    pen = 0.06
    for val in dataPixels:
        # print(val[0])
        if val[0] == 255:
            # print(val)
            part = dwg.ellipse(center=(val[1] + 1, val[2] + 0.5), r=(.5, .5), fill='black')
            # part.rotate(rot, center=(val[1] + 0.5, val[2] + 0.5))
            # part = dwg.add(dwg.rect(insert=(val[1], val[2]), size=(1, 1), fill='black'))
            dwg.add(part)
            # rot = rot + 1
            pen = pen + 0.06

    dwg.saveas('tmp/svg/' + inputFile + '.svg')


# LA MACHINE

font = fontforge.open(sys.argv[1])
size = sys.argv[2]
path = 'tmp'

# shutil.rmtree(path)
# os.mkdir('tmp', 755)

# EXPORT BITMAP GLYPH
for gly in font.glyphs():
    gly.export("tmp/bd/" + gly.glyphname + ".xbm", int(size))
    print(gly.glyphname)

# BUILD SVG
for bitGly in glob.glob('tmp/bd/*.xbm'):
    name = bitGly.split("/")[-1].replace('.xbm', '')
    buildSvg(name)
    print(name)
#
# # Path To Stroke
#
# os.system('bash path.sh')
#
# # BUILD FONT
SVG_DIR = glob.glob('tmp/svg/*.svg')
font.em = int(size)
for glyph in SVG_DIR:
    letter = glyph.split("/")[-1].replace(".svg", "")
    print(letter)
    # im = Image.open('tmp/svg/' + letter + '.svg', 'r')
    zone = font[letter]
    # chasse = im.width
    chasse = zone.width
    zone.clear()
    letter_char = font.createMappedChar(letter)
    letter_char.width = int(chasse)
    letter_char.importOutlines(glyph)
    # letter_char.simplify(10)

font.em = 1000
font.generate('output/test' + size + '.sfd')
