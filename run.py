import fixedFont

# pth = []
# pth.append(fxft.get.path('input/rond.svg'))
# pth.append(fxft.get.path('input/module1.svg'))
fixedFont.load().font('input/Alegreya.otf')
fixedFont.export().letters(10)
fixedFont.generate().svg('ellipse', 'output/bitmap/*.xbm', width=1, height=1, rotate=0)
fixedFont.generate().font('demo-rect2')
