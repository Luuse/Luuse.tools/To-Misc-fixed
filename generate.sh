#!/usr/bin/env bash

rm -f tmp/hd/*.xpm
rm -f tmp/svg/*.svg

for glyph in tmp/bd/*.xbm
do
  file=`basename $glyph .xbm`
  echo $file
  convert tmp/bd/$file'.xbm' -scale 1000% tmp/hd/$file.xpm
done

for glyph in tmp/hd/*.xpm
do
  file=`basename $glyph .xpm`
  echo $file
  autotrace -output-file tmp/svg/$file.svg -output-format svg --color-count 2 tmp/hd/$file.xpm
done
