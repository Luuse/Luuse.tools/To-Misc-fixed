# To Misc fixed
-> Luuse 2017
Transform vector fonts (otf, ttf, sfd... ) to misc fixed fonts. 

## Requirement

  * python
    * modules -> fontforge, lxml, svgwrite
  * imagemagick
  * autotrace

## Usage

`$ bash exe.sh input/fontname.format (pixel value)`

Exemple: 
`$ bash exe.sh input/OldStandardTT-Regular.sfd  40`

