#! /usr/bin/env python
# -*- coding: utf-8 -*-

from PIL import Image
import svgwrite


def buildSvg(inputFile):

    im = Image.open(inputFile, 'r')
    listPix = list(im.getdata())
    dataPixels = []
    count = 0

    dwg = svgwrite.Drawing('file.svg', size=(im.width, im.height))
    dwg.viewbox(0, 0, im.width, im.height)

    for i in range(im.height):
        for u in range(im.width):
            dataPixels += [(listPix[count], u, i)]
            count = count + 1

    rot = 45
    pen = 0.06
    for val in dataPixels:
        print(val[0])
        if val[0] == 255:
            # print(val)
            part = dwg.ellipse(center=(val[1] + 0.5, val[2] + 0.5), r=(0.5, 1), fill='black')
            part.rotate(rot, center=(val[1] + 0.5, val[2] + 0.5))
            # dwg.add(dwg.rect(insert=(val[1], val[2]), size=(6, 1), fill='black'))
            dwg.add(part)
            # rot = rot + 1
            pen = pen + 0.06

    dwg.save()
