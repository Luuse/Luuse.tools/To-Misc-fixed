#!/usr/bin/env bash

for gly in tmp/svg/*.svg;
do
  file=`basename $gly .svg`
  inkscape --verb EditSelectAllInAllLayers \
          --verb SelectionUnGroup \
          --verb SelectionUnion \
          --verb FileSave \
          --verb FileClose \
          --verb FileQuit \
    tmp/svg/$file.svg
done
